﻿namespace Homework_5
{
    internal class ConcatText
    {
        //2. Используя метод вывода значения в консоль, выполните конкатенацию слов и выведите на экран следующую фразу:

        //Welcome to the TMS lesons.

        //Каждое слово должно быть записано отдельно и взято в кавычки, например "Welcome". Не забывайте о пробелах после каждого слова

        public static string ConcatWelcomeString()
        {
            string wordWelcome = "Welcome";
            string wordTo = "to";
            string wordthe = "the";
            string wordTms = "TMS";
            string wordlesson = "lessons.";
            return $"{wordWelcome} {wordTo} {wordthe} {wordTms} {wordlesson}";
        }
    }
}
