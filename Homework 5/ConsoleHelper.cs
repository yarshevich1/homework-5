﻿namespace Homework_5
{
    internal class ConsoleHelper
    {
        public static void PrintTest(string message, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
