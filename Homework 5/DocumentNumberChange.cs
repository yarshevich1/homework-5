﻿using System.Text;
using System.Text.RegularExpressions;

namespace Homework_5
{
    internal class DocumentNumberChange
    {
        //5. Написать программу со следующим функционалом:
        //На вход передать строку(будем считать, что это номер документа).
        //Номер документа имеет формат xxxx-yyy-xxxx-yyy-xyxy, где x — это число,
        //а y — это буква.
        //- Вывести на экран в одну строку два первых блока по 4 цифры.
        //- Вывести на экран номер документа, но блоки из трех букв заменить
        //на*** (каждая буква заменятся на*).
        //- Проверить содержит ли номер документа последовательность abc и
        //вывети сообщение содержит или нет(причем, abc и ABC считается
        //одинаковой последовательностью).
        //- Проверить начинается ли номер документа с последовательности
        //555.
        //- Проверить заканчивается ли номер документа на
        //последовательность 1a2b.
        //Все эти методы реализовать в отдельном классе в статических методах,
        //которые на вход(входным параметром) будут принимать вводимую на
        //вход программы строку.


        // string pattern for change 
        private static readonly string _regexPattern = "[a-zA-Z]";

        public static void PrintAllChanges(string inputText)
        {
            PrintDoubleBlocks(inputText);
            PrintDocumentNumberAndChangeChar(inputText);
            CheckContainsWord(inputText);
            CheckStartWith(inputText);
            CheckEndWith(inputText);
        }

        //Вывести на экран в одну строку два первых блока по 4 цифры
        private static void PrintDoubleBlocks(string inputText)
        {
            var tempBlocks = inputText.Split('-');

            for (int i = 0, count = 0; i < tempBlocks.Length; i++)
            {
                if(count >= 2)
                    break;
                if (int.TryParse(tempBlocks[i], out int numbers) && tempBlocks[i].Length == 4)
                    Console.Write(tempBlocks[i]);
            }

            Console.WriteLine();
        }

        //- Вывести на экран номер документа, но блоки из трех букв заменить
        //на*** (каждая буква заменятся на*).
        private static void PrintDocumentNumberAndChangeChar(string inputText)
        {
            var tempBlocks = inputText.Split('-');

            for (int i = 0; i < tempBlocks.Length; i++)
            {
                if (tempBlocks[i].Length == 3)
                    tempBlocks[i] = Regex.Replace(tempBlocks[i], _regexPattern, "*");
            }

            Console.WriteLine(string.Join("-", tempBlocks));
        }

        //- Проверить содержит ли номер документа последовательность abc и
        //вывети сообщение содержит или нет(причем, abc и ABC считается
        //одинаковой последовательностью).
        private static void CheckContainsWord(string inputText) => 
            Console.WriteLine(inputText.ToLower().Contains("abc") 
                ? $"Text {inputText} contains abc" 
                : $"Text {inputText} doesn't contain abs");

        //- Проверить начинается ли номер документа с последовательности
        //555.
        private static void CheckStartWith(string inputText) =>
            Console.WriteLine(inputText.ToLower().StartsWith("555") 
                ? $"Text {inputText} starts with 555" 
                : $"Text {inputText} doesn't start with 555");

        //- Проверить заканчивается ли номер документа на
        //последовательность 1a2b.
        private static void CheckEndWith(string inputText) => Console.WriteLine(inputText.ToLower().EndsWith("1a2b")
            ? $"Text {inputText} ends with 1a2b"
            : $"Text {inputText} doesn't end with 1a2b");
    }
}
