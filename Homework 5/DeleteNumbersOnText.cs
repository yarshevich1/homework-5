﻿using System.Text.RegularExpressions;

namespace Homework_5
{
    internal class DeleteNumbersOnText
    {
        //1. Заменить в строке все вхождения 'test' на 'testing'. Удалить из текста все символы, являющиеся цифрами.

        // pattern for search number
        private static readonly string _regexPattern = "\\d";

        /// <summary>
        /// Method replace wold 'test' and delete all numbers
        /// </summary>
        /// <param name="inputText">Text to replace</param>
        /// <returns>Changed text</returns>
        public static string ReplaceIntup(string inputText) => Regex.Replace(inputText, _regexPattern, "")
            .Replace("test", "testing");
    }
}
