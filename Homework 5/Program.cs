﻿namespace Homework_5
{
    internal class Program
    {
        // Test data for Task 1
        static string[] _testInputs = {
            "This is a test123 with some digits456.",
            "The quick brown fox jumps over the lazy dog.",
            "12345 testing 67890 numbers in this text.",
            "NoTestHere",
            "AnotherTest",
            "123",
            "TestingTesting",
            "9876543210",
            ""
        };

        // Test data for Task 3
        static string[] _testInputTaskThree = {
            "teamwithsomeofexercisesabcwanttomakeitbetter",
            "abcnotfound",
            "thisisabctestingsomeotherstuff",
            "abcdefghijklmnopqrstuvwxyz",
            "123abc456def789"
        };

        // test data for Task 5
        static string[] _testDocumentNumbers = {
            "1234-abc-5678-xyz-1a2b",    // Валидный
            "4567-def-5432-uvw-3c4d",    // Валидный
            "7890-ghi-8765-rst-5e6f",    // Валидный
            "4321-jkl-4321-opq-7g8h",    // Валидный
            "1357-mno-9753-lmn-9i0j",    // Валидный
            "8642-pqr-7890-ghi-1k2l",    // Валидный
            "2w22-stu-3e33-vwx-3m4n",    // Невалидный (в блоке 'x' есть символ)
            "5555-vyz-6666-tuv-5o6p",    // Валидный
            "9999-abc-0000-abc-7q8r",    // Валидный
            "1111-xyz-2222-xyz-9s0t",    // Валидный
            "12345-abc-5678-xyz-1a2b",   // Невалидный (5 цифр в блоке 'x')
            "abcd-efg-hijk-lmno-pqrs",   // Невалидный (нет блоков 'x' и 'y')
            "1234-ab-5678-xyz-1a2b",     // Невалидный (2 буквы в блоке 'y')
            "1234-abc-5678-xyz-1a2",     // Невалидный (2 цифры в блоке 'xy')
            "12ab-xyz-5678-def-1a2b"     // Невалидный (блоки разной длины)
        };

        static void Main(string[] args)
        {
            ConsoleHelper.PrintTest("Task - 1", ConsoleColor.Cyan);
            foreach (var testInput in _testInputs)
            {
                Console.WriteLine($"{testInput} - {DeleteNumbersOnText.ReplaceIntup(testInput)}");
            }

            Console.WriteLine();
            ConsoleHelper.PrintTest("Task - 2", ConsoleColor.Cyan);
            Console.WriteLine(ConcatText.ConcatWelcomeString());

            ConsoleHelper.PrintTest("Task - 3", ConsoleColor.Cyan);
            foreach (var text in _testInputTaskThree)       
            {
                Tuple<string, string> result = SplitText.SplitTextToAbc(text);
                Console.WriteLine($"First part: { result?.Item1}");
                Console.WriteLine($"Second part: { result?.Item2}");
            }
            Console.WriteLine();

            ConsoleHelper.PrintTest("Task - 4", ConsoleColor.Cyan);
            Console.WriteLine(ChangeBadDay.ChangeDay("Плохой день"));

            Console.WriteLine();

            ConsoleHelper.PrintTest("Task - 5", ConsoleColor.Cyan);

            while (true)
            {
                Console.WriteLine("Enter document number: ");
                var docNumber = Console.ReadLine();
                if (!string.IsNullOrEmpty(docNumber))
                {
                    DocumentNumberChange.PrintAllChanges(docNumber);
                }

                Console.WriteLine("Enter 'q' to exit or press 'Enter' to continue");

                ConsoleKeyInfo keyInfo = Console.ReadKey();
                if (keyInfo.Key == ConsoleKey.Q)
                    break;
            }

            foreach (var test in _testDocumentNumbers)  
            {
                DocumentNumberChange.PrintAllChanges(test);
            }

            Console.WriteLine();
        }
    }
}