﻿using System.Globalization;

namespace Homework_5
{
    internal class SplitText
    {
        //3. Дана строка: teamwithsomeofexcersicesabcwanttomakeitbetter.

        //Необходимо найти в данной строке "abc", записав всё что до этих символов в первую переменную, а также всё, что после них во вторую.

        //Результат вывести в консоль.

        /// <summary>
        /// Split text to abc text
        /// </summary>
        /// <param name="inputText">Intup text</param>
        /// <returns>Return tuple with 2 string value</returns>
        public static Tuple<string, string>? SplitTextToAbc(string inputText)
        {
            string[] tempArray = inputText.ToLower().Split("abc");
            if (tempArray.Length > 0)
            {
                Tuple<string, string> splitText = new Tuple<string, string>(tempArray[0], tempArray[1]);
                return splitText;
            }
            return default;
        }
    }
}
